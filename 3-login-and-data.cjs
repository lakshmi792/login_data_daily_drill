const fs = require('fs');

/*Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well) */

function problem1() {
    return new Promise((resolve, reject) => {
        fs.writeFile('./file1.txt', "File1 is created", (err) => {
            if (err) {
                reject(err);
            } else {
                resolve("created file1")
                console.log("file1 is created")
            }
        })
        return new Promise((resolve, reject) => {
            fs.writeFile('./file2.txt', "File2 is created", (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve("created file2")
                    console.log("file2 is created")
                }
            })
            setTimeout(() => {
                return new Promise((resolve, reject) => {
                    fs.unlink('./file1.txt', (err) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve("file1 is deleted")
                            console.log("file1 is deleted")
                        }
                    })
                    return new Promise((resolve, reject) => {
                        fs.unlink('./file2.txt', (err) => {
                            if (err) {
                                reject(err);
                            } else {
                                resolve("file2 is deleted")
                                console.log("file2 is deleted")
                            }
                        })
                    })
                })
            }, 2 * 1000);
        })
    })
}

//problem1()

/*Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/

function problem2() {
    return new Promise((resolve, reject) => {
        fs.readFile('./lipsum.txt', 'utf-8', (err, data) => {
            if (err) {
                reject(err)
            }
            else {
                resolve(data);
            }
        })
        return new Promise((resolve, reject) => {
            fs.writeFile('./newData.txt', './lipsum.txt', (err) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve("written lispsum data to the new data file");
                }
            })
        })
    })
}


